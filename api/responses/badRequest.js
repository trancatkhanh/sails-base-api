/**
 * Module dependencies
 */

// const util = require('util');
// const _ = require('@sailshq/lodash');



/**
 * 400 (Bad Request) Handler
 *
 * Usage:
 * return res.badRequest();
 * return res.badRequest(data);
 *
 * e.g.:
 * ```
 * return res.badRequest(
 *   'Please choose a valid `password` (6-12 characters)',
 *   'trial/signup'
 * );
 * ```
 */

module.exports = function badRequest(message) {
  sails.log.debug('badRequest()', {
    message,
    fingerprint: this.req.fingerprint.hash
  });

  // if(!message || !data){
  //   throw flaverr({
  //     code: 'WRONG_USAGE',
  //     message: this.req.i18n.__('WRONG_USAGE'),
  //   })
  // }
  const status = 400;
  const res = this.res;

  const error = {
    code: 'BAD_REQUEST',
    message: message ? this.req.i18n.__(message) : 'BAD_REQUEST',
    status
  };
  res.status(status).json({error});
};

