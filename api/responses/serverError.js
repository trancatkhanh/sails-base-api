/**
 * Module dependencies
 */

// var _ = require('@sailshq/lodash');
const flaverr = require('flaverr');

const { E_DEFAULT_HANDLER, E_DEFAULT_MESSAGE } = sails.config.constants

const { JWT_ERROR } = sails.config.constants

/**
 * 500 (Server Error) Response
 *
 * Usage:
 * return res.serverError();
 * return res.serverError(err);
 * return res.serverError(err, 'some/specific/error/view');
 *
 * NOTE:
 * If something throws in a policy or controller, or an internal
 * error is encountered, Sails will call `res.serverError()`
 * automatically.
 */


module.exports = function serverError (data) {
  const parseError = flaverr.parseError(data) || data;
  let status = _.get(parseError, 'status', 400);
  // status = (status === 500) ? 400 : status; // dont set status to 500 because we already handle it

  let errorName = _.get(parseError, 'name')
  let errorCode = _.get(parseError, 'code')
  let errorMessage = _.get(parseError, 'message')
  let error = {
    code: errorCode || errorName || E_DEFAULT_HANDLER,
    message: errorMessage ? this.req.i18n.__(errorMessage) : this.req.i18n.__(E_DEFAULT_MESSAGE),
    status
  };

  // For JWT error
  if(errorName && JWT_ERROR[errorName]){
    error = {
      ...error,
      ...JWT_ERROR[errorName]
    }
    status = JWT_ERROR[errorName].status
  }

  sails.log.debug('serverError()', {
    data,
    parseError,
    fingerprint: this.req.fingerprint,
    error
  });

  return this.res.status(status).send({error});
};
