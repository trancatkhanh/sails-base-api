/**
 * notFound.js
 *
 * A custom response.
 *
 * Example usage:
 * ```
 *     return res.notFound();
 *     // -or-
 *     return res.notFound(optionalData);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'notFound'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */

module.exports = function data({message, data={}}) {
  sails.log.debug('data()', {
    message, data,
    fingerprint: this.req.fingerprint.hash
  });

  // if(!message || !data){
  //   throw flaverr({
  //     code: 'WRONG_USAGE',
  //     message: this.req.i18n.__('WRONG_USAGE'),
  //   })
  // }
  let res = this.res;

  res.status(200).json({
    message: message ? this.req.i18n.__(message) : '',
    data
  });
};
