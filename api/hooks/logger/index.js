/**
 * Module dependencies.
 */

var CaptainsLog = require('captains-log');


module.exports = function(sails) {


  /**
   * Expose `logger` hook definition
   */

  return {


    defaults: {
      log: {
        level: 'info'
      }
    },


    configure: function() {

    },


    /**
     * Initialize is fired when the hook is loaded,
     * but after waiting for user config.
     */

    initialize: function(cb) {

      // Get basic log functions
      /// eslint-disable-next-line
      const log = CaptainsLog(sails.config.log);

      // console.log('sails.config.log', sails.config.log);

      // sails.on('router:before', function (){
      //   sails.router.bind('all /*', (req, res, next) => {
      //     console.log('router bind', {
      //       fp: req.fingerprint
      //     });
      //     // console.log('router bind', {
      //     //   req
      //     // });
      //     next()
      //     // return sails.hooks.i18n.expressMiddleware(req, res, next);
      //   });
      //
      // });//</sails.on>

      // // Mix in log.ship() method
      // log.ship = buildShipFn(
      //   sails.version ? ('v' + sails.version) : '',
      //   log.info
      // );

      // Expose log on sails object
      sails.log = log;
      sails.log.debug('Service Started');
      return cb();
    }

  };
};
