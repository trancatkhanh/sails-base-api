const passport = require('passport')

module.exports = {
  profile: async (req, res) => {
    // req.checkRole('Admin')
    res.send({
      data: req.user
    })
  },

  server: async (req, res) => {
    // req.checkRole('Admin')

    res.send({
      header: req.headers,
      connection: req.connection.server,
      remoteAddress: req.connection.remoteAddress,
    })
  },

  sessions: async (req, res) => {
    const sessions = await Employees.getSessions(req.user)
    res.send({
      data: sessions
    })
  },

  deleteAllsessions: async (req, res) => {
    const sessions = await Employees.deleteAllSessions(req.user)
    res.send({
      data: sessions
    })
  },

  // DELETE auth/sessions/:token/:hash
  deleteSessions: async (req, res) => {
    await Validate({schema: Employees.deleteSessionsValidationSchema , params: req.allParams()})
    let token = _.get(req.allParams(), 'token')
    let hash = _.get(req.allParams(), 'hash')
    let user = _.get(req, 'user')
    const sessions = await Employees.deleteSessions(user, token, hash)

    res.send({
      data: sessions
    })
  },

  login: async(req, res) => {
    const fingerprint = _.get(req, 'fingerprint')
    await Validate({schema: Employees.loginValidationSchema , params: {
      ...req.allParams(),
      fingerprint
    }})
    passport.authenticate('local', (err, employee) => {
      if((err) || (!employee)) {
        return res.serverError({
          ...err,
          status: 403
        })
      }
      // console.log('employee', employee);
      return res.data({
        message: 'LOGIN_SUCCESS',
        data: employee
      });
    })(req, res)
  },

  register: async(req, res) => {
    let params = req.allParams()
    const registered = await Employees.createAccount(params)
    return res.data(registered)
  },

  refresh: async(req, res) => {
    sails.log.debug('refresh()', {
      user: req.user
    });
    // let registerData = req.allParams()
    res.data({
      message: 'Refresh Token success',
      data: req.user
    });

  },

  logout: async(req, res) => {
    req.logout()
    res.data({message: 'You are logged out'})
  },

  forgotPassword: async(req,res) => {
    const params = req.allParams()
    const forgotPassword = await Employees.forgotPassword(params)
    return res.data(forgotPassword)
  },

  fetchEmailByToken: async(req,res) => {
    const params  = req.allParams()
    const fetchEmail = await Employees.fetchEmailByToken(params)
    return res.data(fetchEmail)
  },

  resetPassword: async(req,res) => {
    const params = req.allParams()

    const updateData = {
      email: _.get(params, 'email'),
      password: _.get(params, 'password'),
      endSession: _.get(params, 'endSession', true),
      token: _.get(params, 'token'), //parse token instead of generate inside function
      headers: _.get(req, 'headers'),
      fingerprint: _.get(req, 'fingerprint'),
      remoteAddress: _.get(req, 'connection.remoteAddress'),
    }

    const updated = await Employees.updatePasswordWithToken(updateData)
    return res.data(updated)
  },


  // google: async (req, res, next) => {
  //   const scopeAccess = 'openid profile email'
  //   passport.authenticate('google', {
  //     scope: scopeAccess
  //   })(req, res, next)
  // },
  //
  // googleCallback: async(req, res, next) => {
  //   passport.authenticate('google', (err, user) => {
  //     req.logIn(user,(err)=>{
  //       if(err) res.send(err)
  //       return res.json(user);
  //     })
  //   })(req, res, next);
  // },

  // facebook: async (req, res, next) => {
  //   passport.authenticate('facebook')(req, res, next)
  // },
  //
  // facebookCallback: async(req, res, next) => {
  //   passport.authenticate('facebook', (err, user) => {
  //     req.logIn(user,(err)=>{
  //       if(err) res.send(err)
  //       return res.json(user);
  //     })
  //   })(req, res, next);
  // },
}
