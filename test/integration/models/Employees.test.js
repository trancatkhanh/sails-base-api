const bcrypt = require('bcrypt-nodejs');
const headers =
    { 'accept-encoding': 'gzip, deflate, br',
      referer:
        'http://localhost:8000/user/login?redirect=http%3A%2F%2Flocalhost%3A8000%2Fadmin%2Froles%2Fdetails%2F2',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      origin: 'http://localhost:8000',
      'content-type': 'application/json;charset=UTF-8',
      'user-agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
      'accept-language': 'vi-VN',
      'sec-fetch-dest': 'empty',
      accept: 'application/json',
      'content-length': '60',
      connection: 'close',
      host: 'localhost:1337' }
  const remoteAddress = '::ffff:127.0.0.1'
  const fingerprint =
    {
      hash: '5166f2e3bd8a9efc1faf8510b2ec06cd',
      components:
        {
          useragent: {
            browser: { family: 'Chrome', version: '80' },
            device: { family: 'Other', version: '0' },
            os: { family: 'Mac OS X', major: '10', minor: '15' } },
          geoip: { country: null }
        }
    }
let endSession = true
describe('Employee (model)', () => {
  describe('#login()', () => {
    it('should throw [x] error when validate fail', async () => {
      await Employees.createAccount({
        name: 'Doraemon',
        email: 'doraemon@shopdongho.io',
        password: "123456!!!",
        store: 4,
        gender: 'male',
        active: true,
        role: [2]
      }).should.be.rejectedWith('Your password is not complex enough');

      await Employees.login({
        email: '',
        password: "12345678x",
        headers,
        fingerprint
      }).should.be.rejectedWith('"email" is not allowed to be empty');

      await Employees.login({
        email: 'admin@shopdongho.io',
        password: "",
        fingerprint
      }).should.be.rejectedWith('"password" is not allowed to be empty');
    });

    it('should throw [x] error when invalid auth', async () => {
      let employee = await Employees.login({
        email: 'admin@shopdongho.io',
        password: "12345678x",
        fingerprint
      }).should.be.rejectedWith('E_INVALID_AUTH');
    });

    it('should throw [x] error when user not found', async () => {
      let employee = await Employees.login({
        email: 'adminx@shopdongho.io',
        password: "12345678x",
        fingerprint
      }).should.be.rejectedWith('E_USER_NOT_FOUND');
    });

    it('should return [v] employee', async () => {
      let employee = await Employees.login({
        email: 'admin@shopdongho.io',
        password: "SuperPass123!!!",
        headers,
        fingerprint
      });

      // console.log('employee', employee);
      sails.log.debug('login()', {
        employee,
        keys: Object.keys(employee),
        fingerprint
      });
      employee.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
    });

    it('should return [v] new employee data', async () => {
      const employee = await Employees.createAccount({
        name: 'Doraemon',
        email: 'doraemon@shopdongho.io',
        password: "SuperPass123!!!",
        gender: 'male',
        store: 4,
        active: true,
        role: [2]
      })
      console.log('employee.data', employee.data);
      employee.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");

      let employeeLogin = await Employees.login({
        email: 'doraemon@shopdongho.io',
        password: "SuperPass123!!!",
        headers,
        fingerprint
      });
      employeeLogin.should.have.keys("token")
      await Employees.destroy({email: 'doraemon@shopdongho.io'})
    });


    it('should return all alive token', async () => {
      // const employee = await Employees.createAccount({
      //   name: 'Jerry Mouse',
      //   email: 'admin@gmail.com',
      //   password: "SuperPass123!!!"
      // })
      // employee.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");

      let employeeLogin = await Employees.login({
        email: 'admin@shopdongho.io',
        password: "SuperPass123!!!",
        headers,
        fingerprint
      });
      const sessions = await Employees.getSessions(employeeLogin)
      console.log('employeeLogin', {
        employeeLogin,
        sessions
      });
      // employeeLogin.should.have.keys("token")
      // await Employees.destroy({email: 'admin@gmail.com'})
    });
  });
  describe('#search()', () => {
    it('should throw [x] error when enter params is not allow', async () => {
      const employee = await Employees.search({
        filter: 'name',
        keyword: "admin",
        id: 1, name: 'admin'
      }).should.be.rejectedWith('"name" is not allowed');
    });

    it('should return [v] employee when search success', async () => {
      const employee = await Employees.search({
        filter: 'name',
        keyword: ""
      })
      console.log('employee', employee);
      employee.data.employeeList[0].should.have.keys("id","name","email","phone","address","active","store","role");
    });
  });
  describe('#update()', () => {
    it('should throw [x] error when enter params is not allow', async () => {
      const employee = await Employees.updateEmployee({
        id: 10,
        first_name: 'Mickey',
        last_name: 'Mouse'
      }).should.be.rejectedWith('"first_name" is not allowed. "last_name" is not allowed');
      // console.log('employee', employee);
    });

    it('should throw [x] error when new password is not complex enough ', async () => {
      const employee = await Employees.updateEmployee({
        id: 10,
        newPass: "12345678",
      }).should.be.rejectedWith('Your new password is not complex enough')
    });

    it('should throw [x] error when enter empty name', async () => {
      const employee = await Employees.updateEmployee({
        id: 10,
        name: '',
      }).should.be.rejectedWith('"name" is not allowed to be empty')
    });

    it('should throw [x] error when missing params (id)', async () => {
      const employee = await Employees.updateEmployee({
        name: 'Mickey',
        newPass: "newPassword!2#",
      }).should.be.rejectedWith('"id" is required')
    });

    it('should return [v] employee when update info success', async () => {
      const employee = await Employees.updateEmployee({
        id: 10,
        newPass: "newPassword!2#",
        name: 'Mickey Mouse'
      })
      employee.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
    });

    it('should return [v] employee when login with new pass success', async () => {
      it('should return employee', async () => {
        const employee = await Employees.login({
          email: 'admin@shopdongho.io',
          password: "newPassword!2#"
        });
        employee.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
      });

      await Employees.updateEmployee({
        id: 10,
        newPass: "SuperPass123!!!"
      })
    });

    it('should throw [v] employee when update name success', async () => {
      const employee = await Employees.updateEmployee({
        id: 10, name: 'Donald Duck'
      })
      employee.data.should.have.properties({name: 'Donald Duck'})

      const reUpdated = await Employees.updateEmployee({
        id: 10, name: 'Mickey Mouse'
      })
      reUpdated.data.should.have.properties({name: 'Mickey Mouse'})
    });

    it('should throw [v] employee when disabled success', async () => {
      const employee = await Employees.updateEmployee({
        id: 11, active: false
      })
      employee.data.should.have.properties({active: false})

      await Employees.login({
        email: 'donald@shopdongho.io',
        password: 'SuperPass123!!!',
        fingerprint
      }).should.be.rejectedWith('E_USER_DISABLED')

      const reUpdated = await Employees.updateEmployee({
        id: 11, active: true,
      })
      reUpdated.data.should.have.properties({active: true})
    });

    it('should return [v] employee & check newPassworUpdated when update password success', async () => {
      let employeeData = await Employees.login({
        email: 'admin@shopdongho.io',
        password: "SuperPass123!!!",
        headers,
        fingerprint
      });

      let oldPasswordHash = employeeData.password
      let oldPassword = "SuperPass123!!!"
      let password = "SuperPass123!!"
      let id = employeeData.id

      const employee = await Employees.updatePassword({ fingerprint, oldPassword, password, oldPasswordHash, id, endSession})


      const newPasswordUpdated = bcrypt.compareSync(password, employee.data.password)

      expect(newPasswordUpdated).toBe(true)

      // Relogin with new password
      let employeeReLoginData = await Employees.login({
        email: 'admin@shopdongho.io',
        password: "SuperPass123!!",
        headers,
        fingerprint
      });

      employeeReLoginData.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");


      async function revertPassword(){
        let employeeData = await Employees.login({
          email: 'admin@shopdongho.io',
          password: "SuperPass123!!",
          headers,
          fingerprint
        });

        let oldPasswordHash = employeeData.password
        let oldPassword = "SuperPass123!!"
        let password = "SuperPass123!!!"
        let id = employeeData.id

        let employee = await Employees.updatePassword({ fingerprint, endSession, oldPassword, password, oldPasswordHash, id})


        let newPasswordUpdated = bcrypt.compareSync(password, employee.data.password)

        sails.log.debug('revertPassword()', {
          message: 'reverted'
        });
        expect(newPasswordUpdated).toBe(true)
      }
      await revertPassword()
    });

  });
  describe('#Fetch', () => {
    it('should throw [x] error when get employee details with invalid params', async () => {
      const employee = await Employees.getEmployee({
        id: 10,
        name: 'admin'
      }).should.be.rejectedWith('"name" is not allowed')
    });

    it('should throw [x] error when fetch employees of store with invalid params', async () => {
      const employee = await Employees.fetchAllByStoreId({
        id: 3,
        name: 'Shop'
      }).should.be.rejectedWith('"name" is not allowed')
    });

    it('should return [v] employees belong to store', async () => {
      const employee = await Employees.fetchAllByStoreId({
        id: 3
      })
      employee.data[0].should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
      employee.data[0].should.have.property('store', 3);
    });

    it('should return [v] employee list', async () => {
      const employee = await Employees.search({
        page: 1,
        limit: 20
      })
      employee.data.employeeList[0].should.have.keys("id","name","email","phone","address","active","store","role");
    });

    it('should return [v] employee when get employee details', async () => {
      const employee = await Employees.getEmployee({
        id: 10
      })
      employee.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
    });
  });
});
