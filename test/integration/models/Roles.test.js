const caslAbility = require('@casl/ability')
const caslAbilityExtra = require('@casl/ability/extra')
const { AbilityBuilder, Ability } = caslAbility
const { packRules } = caslAbilityExtra

describe('Roles (model)', () => {
  describe('#Create()', () => {
    it('should throw error when invalid params', async () => {
      const role = await Roles.createNew({
        name: 'SuperMod',
        type: 'Mod',
        access: [],
        active: true
      }).should.be.rejectedWith('"type" is not allowed')
    })

    it('should throw error when access wrong type', async () => {
      const role = await Roles.createNew({
        name: 'SuperMod',
        access: '[]',
        active: true
      }).should.be.rejectedWith('"access" must be an array')
    })

    it('should throw error when existed role name', async () => {
      const role = await Roles.createNew({
        name: 'Admin',
        access: [],
        active: true
      }).should.be.rejectedWith('E_FOUND_EXISTING')
    })

    it('should return role data when created success', async () => {
      const role = await Roles.createNew({
        name: 'SuperMod',
        access: [{"key":"customer","access":["GET.customer"]}],
        active: true
      })

      let { ability } = await Roles.roleToAbility([role.data.id])
      const can_view_customer = ability.can('get', '/customer')
      const can_not_view_store = ability.cannot('get', '/store')
      const can_not_edit_customer = ability.cannot('put', '/customer')

      role.data.should.be.has.properties("createdAt", "updatedAt", "id", "name", "access", "active")
      role.data.name.should.be.equal('SuperMod')

      expect(can_not_view_store).toBe(true)
      expect(can_view_customer).toBe(true)
      expect(can_not_edit_customer).toBe(true)
    })
  });

  describe('#Update', () => {
    it('should throw error when update existed name', async () => {
      const getRole = await Roles.findOne({name: 'SuperMod'})
      const updated = await Roles.updateRole({
        id: getRole.id,
        name: 'Admin',
        access: [],
      }).should.be.rejectedWith('E_FOUND_EXISTING')
    });

    it('should return new role access when updated success', async () => {
      const getRole = await Roles.findOne({name: 'SuperMod'})
      const updated = await Roles.updateRole({
        id: getRole.id,
        name: 'SuperMod',
        access: [{"key":"store","access":["GET.store"]}],
      })

      updated.data.should.be.has.properties("createdAt", "updatedAt", "id", "name", "access", "active")
      updated.data.name.should.be.equal('SuperMod')
      await Roles.destroy({name: 'SuperMod'})
    });
  });

  describe('#Fetch', () => {
    it('should throw error when invalid search params', async () => {
      const role = await Roles.searchById({
        name: 'Mod'
      }).should.be.rejectedWith('"id" is required. "name" is not allowed')
    });

    it('should return role access when fetch success', async () => {
      const role = await Roles.searchById({
        id: 2
      })
      role.data.role.should.be.has.properties("createdAt", "updatedAt", "id", "name", "access", "active")
    });
  });

  // describe('#check role', () => {
  //   it('should use ability to check can PUT order', async () => {
  //     let { ability } = await Roles.roleToAbility([2])
  //
  //     const can_view_order = ability.can('get', '/order')
  //     const can_not_edit_order = ability.cannot('put', '/order')
  //     const can_not_create_order = ability.cannot('post', '/order')
  //
  //     expect(can_view_order).toBe(true)
  //     expect(can_not_edit_order).toBe(true)
  //     expect(can_not_create_order).toBe(true)
  //     // expect(ability.cannot('get', '/role')).toBe(false)
  //   });
  // });
});
