const sinon = require('sinon');
const supertest = require('supertest');


describe('EmployeeController', () => {
  before(() => {
    this.token = null;
    this.newToken = null;
    // console.log('sails.config', sails.config.jwt);
    // sinon.stub(sails.configs.employees, 'findOne').callsFake(async ({ id }) => ({
    //   id: id,
    //   role: 'admin',
    // }));
    // Set token expired in 1 second
    this.sinonStub = sinon.stub(sails.config.jwt.JWT_CONFIG, 'expiresIn').get(function getterFn() {
      return '10 second';
    });


  });
  after(() => {
    this.sinonStub.restore();
  })

  it('#should return token', (done) => {


    supertest(sails.hooks.http.app)
      .post('/auth/login').send({
      email: 'admin@shopdongho.io',
      password: 'SuperPass123!!!'

    }).set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        // console.log('this.token', this.token);
        // expect(res.body.data.email).to.be.a('string');

        expect(res.body.data.email).toEqual('admin@shopdongho.io');

        this.token = res.body.data.token
        return done();
      });
  });



  it('#should response error code E_INVALID_AUTH if wrong password in update password', (done) => {
    console.log('this.token', this.token);
    supertest(sails.hooks.http.app)
      .post('/employee/updatePassword').send({
      password: 'SuperPass123!!!',
      endSession: false,
      oldPassword: 'SuperPass123!!',
    })
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        // this.newToken = res.body.data.token
        console.log('res.body', _.get(res, 'body'));
        // res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(_.get(res, 'body.error.code')).toEqual('E_INVALID_AUTH');
        // res.body.should.deepEqual({
        //   id: 10,
        //   email: 'admin@shopdongho.io',
        //   phone: '0999999999',
        // });
      })
      .expect(401, done);
  });
  it('#should return user with token after update password with endSession: false', (done) => {
    console.log('this.token', this.token);
    supertest(sails.hooks.http.app)
      .post('/employee/updatePassword').send({
      password: 'SuperPass123!!!',
      oldPassword: 'SuperPass123!!!',
      endSession: false, // endSession fasle -> return new token to user for next authenticate
    })
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        // this.newToken = res.body.data.token
        console.log('res.body', _.get(res, 'body'));
        // res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(res.body.data.token).toBeDefined()
        // toEqual(undefined);
        expect(_.get(res, 'body.data')).toMatchObject({
          active: true,
          email: 'admin@shopdongho.io',
        });
        // res.body.should.deepEqual({
        //   id: 10,
        //   email: 'admin@shopdongho.io',
        //   phone: '0999999999',
        // });
      })
      .expect(200, done);
  });
  it('#should return user without token after update password with endSession: true', (done) => {
    console.log('this.token', this.token);
    supertest(sails.hooks.http.app)
      .post('/employee/updatePassword').send({
      password: 'SuperPass123!!!',
      oldPassword: 'SuperPass123!!!',
      endSession: true, // endSession true -> dont return new token to user -> need to be redirect to login page
    })
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        // this.newToken = res.body.data.token
        console.log('res.body', _.get(res, 'body'));
        // res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        // expect(res.body.data.token).toBeUndefined()
        // toEqual(undefined);
        // expect(_.get(res, 'body.data')).toMatchObject({
        //   active: true,
        //   email: 'admin@shopdongho.io',
        // });
        // res.body.should.deepEqual({
        //   id: 10,
        //   email: 'admin@shopdongho.io',
        //   phone: '0999999999',
        // });
      })
      .expect(200, done);
  });

  it('#should get profile when have token', (done) => {
    console.log('this.token', this.token);
    supertest(sails.hooks.http.app)
      .get('/auth/profile')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        console.log('res.body.data', res.body);

        res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(res.body.data.email).toEqual('admin@shopdongho.io');
        // res.body.should.deepEqual({
        //   id: 10,
        //   email: 'admin@shopdongho.io',
        //   phone: '0999999999',
        // });
      })
      .expect(200, done);
  });


  it('#should be refresh new token', (done) => {
    console.log('this.token', this.token)
    supertest(sails.hooks.http.app)
    .get('/auth/refresh')
    .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        sails.log.debug('should be refresh new token()', {
          res
        });
        res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(res.body.data.email).toEqual('admin@shopdongho.io');
      })
    .expect(200, done);
  })



});
