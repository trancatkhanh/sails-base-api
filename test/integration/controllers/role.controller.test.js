const sinon = require('sinon');
const supertest = require('supertest');


describe('RoleController', () => {
  before(() => {
    this.token = null;
    this.newToken = null;
    // console.log('sails.config', sails.config.jwt);
    // sinon.stub(sails.configs.employees, 'findOne').callsFake(async ({ id }) => ({
    //   id: id,
    //   role: 'admin',
    // }));
    // Set token expired in 1 second
    this.sinonStub = sinon.stub(sails.config.jwt.JWT_CONFIG, 'expiresIn').get(function getterFn() {
      return '10 second';
    });


  });
  after(() => {
    this.sinonStub.restore();
  })

  it('#should return token', (done) => {


    supertest(sails.hooks.http.app)
      .post('/auth/login').send({
      email: 'admin@shopdongho.io',
      password: 'SuperPass123!!!'

    }).set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        // console.log('this.token', this.token);
        // expect(res.body.data.email).to.be.a('string');

        expect(res.body.data.email).toEqual('admin@shopdongho.io');

        this.token = res.body.data.token
        return done();
      });
  });



  it('#should return role access', (done) => {
    supertest(sails.hooks.http.app)
      .get('/role/1')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        console.log('res', res);
      })
      // .expect(401, done);
  });



});
