const sinon = require('sinon');
const supertest = require('supertest');


describe('AuthenController.get', () => {
  before(() => {
    this.token = null;
    // console.log('sails.config', sails.config);
    // sinon.stub(sails.configs.employees, 'findOne').callsFake(async ({ id }) => ({
    //   id: id,
    //   role: 'admin',
    // }));
    // this.sinonStub = sinon.stub(sails.config.jwt.JWT_CONFIG, 'expiresIn').get(function getterFn() {
    //   return '10 second';
    // });
  });
  // after(() => {
  //   sails.models.employees.findOne.restore();
  // })

  it('#should return token', (done) => {
    supertest(sails.hooks.http.app)
      .post('/auth/login').send({
      email: 'admin@shopdongho.io',
      password: 'SuperPass123!!!'

    }).set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        // console.log('this.token', this.token);
        // expect(res.body.data.email).to.be.a('string');

        expect(res.body.data.email).toEqual('admin@shopdongho.io');

        this.token = res.body.data.token
        return done();
      });
  });



  it('#should get profile when have token', (done) => {
    console.log('this.token', this.token);
    supertest(sails.hooks.http.app)
      .get('/auth/profile')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(res.body.data.email).toEqual('admin@shopdongho.io');
      })
      .expect(200, done);
  });

  it('#should be refresh new token', (done) => {
    console.log('this.token', this.token)
    supertest(sails.hooks.http.app)
    .get('/auth/refresh')
    .set('Authorization', `Bearer ${this.token}`)
      .expect(res => {
        res.body.data.should.have.keys("createdAt","updatedAt","id","name","slug","gender","birthday","email","phone","password","address","facebookId","googleId","reset_password","reset_password_token","avatar","active","store","role");
        expect(res.body.data.email).toEqual('admin@shopdongho.io');
      })
    .expect(200, done);
  })


});
