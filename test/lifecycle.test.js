require('dotenv').config()
const sails = require('sails');
const should = require('should'); // https://www.npmjs.com/package/should
const expect = require('expect'); // https://jestjs.io/docs/en/expect.html


global.expect = expect
// Before running any tests...
before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift, even if you have a bunch of assets.
  // this.timeout(10000);

  sails.lift({
    // Your sails app's configuration files will be loaded automatically,
    // but you can also specify any other special overrides here for testing purposes.

    // For example, we might want to skip the Grunt hook,
    // and disable all logs except errors and warnings:
    hooks: {
      // i18n: false, // logger: false, //orm: leave default hook
      // blueprints: false,
      // controllers: false,
      // cors: false,
      // csrf: false,
      grunt: false,
      // http: true,
      // policies: false,
      pubsub: false,
      // request: true,
      // responses: false, //services: leave default hook,
      session: false,
      sockets: false,
      // views: false,
      // security: false,
      // orm: true
    },
    log: { level: process.env.LOG_LEVEL || 'verbose' },

  }, (err) => {
    if (err) { return done(err); }

    // here you can load fixtures, etc.
    // (for example, you might want to create some records in the database)

    return done();
  });
});

// After all tests have finished...
after((done) => {

  // here you can clear fixtures, etc.
  // (e.g. you might want to destroy the records you created above)

  sails.lower(done);

});
