const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;


passport.serializeUser((user, cb) => {
  // console.log('user', user);
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  // console.log('deserializeUser', deserializeUser);
  Employees.findOne({id},(err, user) => {
    cb(err, user);
  });
});

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true,
}, async(req, email, password, cb) => {
  const fingerprint = _.get(req, 'fingerprint')
  Employees.login({
    email, password,
    headers: req.headers,
    remoteAddress: _.get(req, 'connection.remoteAddress'),
    fingerprint})
           .then((user) => {
             return cb(null, user, null);
           })
           .catch((e) => {
             // Passport hack
             return cb({ code: e.code, message: e.message}, null, null)
           })
})
)
