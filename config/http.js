/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For more information on configuration, check out:
 * https://sailsjs.com/config/http
 */

const Fingerprint = require('express-fingerprint')
// const request = require('request');

// const IPService = {
//
//   /*
//    * IP Stack Request */
//   ipStackRequest: (ip) => (
//     new Promise((resolve, reject) => {
//
//       let url = 'http://api.ipstack.com/'+ip+'?access_key=<YOUR_ACCESS_KEY>';
//       request({
//         url: url,
//         method: 'GET',
//         json: true
//       }, (error, response, body) => {
//
//         if(response.statusCode !== 200)
//           resolve('FAILED');
//
//         if(body.success === false) {
//           resolve('LIMIT_REACHED');
//         }
//
//         if(typeof body.country_code !== 'undefined') {
//           resolve(body.country_code);
//         }
//         resolve('FAILED');
//       });
//     })
//   ),
//
//   /*
//      * IP Locate Request */
//   ipLocateRequest: (ip) => (
//     new Promise((resolve, reject) => {
//
//       let url = 'https://www.iplocate.io/api/lookup/'+ip;
//       request({
//         url: url,
//         method: 'GET',
//         json: true
//       }, (error, response, body) => {
//
//         if(response.statusCode === 429) {
//           resolve('LIMIT_REACHED');
//         }
//
//         if(response.statusCode !== 200) {
//           resolve('FAILED');
//         }
//
//         if(typeof body.country_code !== 'undefined') {
//           resolve(body.country_code);
//         }
//         resolve('FAILED');
//       });
//     })
//   ),
//
// };
module.exports.http = {

  port: process.env.PORT,
  cache: 365.25 * 24 * 60 * 60 * 1000, // One year
  /***************************************************************************
   *                                                                          *
   * Proxy settings                                                           *
   *                                                                          *
   * If your app will be deployed behind a proxy/load balancer - for example, *
   * on a PaaS like Heroku - then uncomment the `trustProxy` setting below.   *
   * This tells Sails/Express how to interpret X-Forwarded headers.           *
   *                                                                          *
   * This setting is especially important if you are using secure cookies     *
   * (see the `cookies: secure` setting under `session` above) or if your app *
   * relies on knowing the original IP address that a request came from.      *
   *                                                                          *
   * (https://sailsjs.com/config/http)                                        *
   *                                                                          *
   ***************************************************************************/
  trustProxy: true,

  /****************************************************************************
  *                                                                           *
  * Sails/Express middleware to run for every HTTP request.                   *
  * (Only applies to HTTP requests -- not virtual WebSocket requests.)        *
  *                                                                           *
  * https://sailsjs.com/documentation/concepts/middleware                     *
  *                                                                           *
  ****************************************************************************/


  middleware: {

    /***************************************************************************
    *                                                                          *
    * The order in which middleware should be run for HTTP requests.           *
    * (This Sails app's routes are handled by the "router" middleware below.)  *
    *                                                                          *
    ***************************************************************************/

    passportInit    : require('passport').initialize(),
    passportSession : require('passport').session(),

    order: [
      'cookieParser',
      'session',
      'passportInit',
      'passportSession',
      'bodyParser',
      'compress',
      // 'getIP',
      'fp',
      'poweredBy',
      'router',
      'www',
      'favicon',
    ],
    // order: [
    //   'cookieParser',
    //   'session',
    //   'bodyParser',
    //   'compress',
    //   'poweredBy',
    //   'router',
    //   'www',
    //   'favicon',
    // ],


    /***************************************************************************
    *                                                                          *
    * The body parser that will handle incoming multipart HTTP requests.       *
    *                                                                          *
    * https://sailsjs.com/config/http#?customizing-the-body-parser             *
    *                                                                          *
    ***************************************************************************/

    // bodyParser: (function _configureBodyParser(){
    //   var skipper = require('skipper');
    //   var middlewareFn = skipper({ strict: true });
    //   return middlewareFn;
    // })(),

    // getIP: async (req, response, next) => {
    //   // console.log('xcontext', sails.i18n.getLocale());
    //   async function getIP() {
    //
    //
    //     try {
    //
    //       let now = new Date();
    //       let nowUnix = +new Date()/1000;
    //       let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    //
    //       //const serviceLimits = await db.getGeoLimitExpiry();
    //       const serviceLimits = {
    //         ipstack_limit_expiry: 1543035611,
    //         iplocate_limit_expiry: 0
    //       };
    //
    //       //handle IPStack result
    //       if(serviceLimits.ipstack_limit_expiry < nowUnix) {
    //
    //         let result = await IPService.ipStackRequest(ip);
    //         if(result === 'LIMIT_REACHED') {
    //
    //           let limitExpire = +new Date(now.getFullYear(), now.getMonth()+1, 1+1)/1000;
    //           //db.updateGeoLimitExpiry('ipstack', limitExpire);
    //
    //         } else if(result !== 'FAILED') {
    //           return {country: result}
    //         }
    //       }
    //
    //       //handle IPLocate result
    //       if(serviceLimits.iplocate_limit_expiry < nowUnix) {
    //
    //         result = await IPService.ipLocateRequest(ip);
    //         if(result === 'LIMIT_REACHED') {
    //
    //           let limitExpire = +new Date(now)/1000 + (60*60*24);
    //           //db.updateGeoLimitExpiry('iplocate', limitExpire);
    //
    //         } else if(result !== 'FAILED') {
    //           return {country: result}
    //           // return;
    //         }
    //       }
    //
    //       //Geo IP fallback
    //       const geo = geoip.lookup(ip);
    //
    //       return {country: geo.country}
    //
    //       // console.log('', {
    //       //
    //       // });
    //       // next();
    //     } catch(e) {
    //       console.log('e', {
    //         e
    //       });
    //       // next(e);
    //     }
    //   }
    //   let ip = await getIP()
    //
    //   console.log('ip', {
    //     ip
    //   });
    //   next()
    //
    // },

    fp: Fingerprint({
      parameters:[
        // Defaults
        Fingerprint.useragent,
        // Fingerprint.acceptHeaders,
        Fingerprint.geoip,

        // Additional parameters
        // function(next) {
        //   next(null,{
        //     'devideId':123123123
        //   })
        // },
      ]
    })


  },

};
