/**
 * Global Variable Configuration
 * (sails.config.globals)
 *
 * Configure which global variables which will be exposed
 * automatically by Sails.
 *
 * For more information on any of these options, check out:
 * https://sailsjs.com/config/globals
 */

const ERROR_CONST = {

  // Server Error
  S_INTERNAL_ERROR: 'S_SOMETHING_WRONG',

  // Basic Error
  E_INVALID_TOKEN: 'E_INVALID_TOKEN',
  E_PERMISSION_DENIED: 'E_PERMISSION_DENIED',
  E_INVALID_AUTH: 'E_INVALID_AUTH',
  E_USER_NOT_FOUND: 'E_USER_NOT_FOUND',
  E_BRAND_NOT_FOUND: 'E_BRAND_NOT_FOUND',
  E_DATA_NOT_FOUND: 'E_DATA_NOT_FOUND',
  E_FOUND_EXISTING: 'E_FOUND_EXISTING',
  E_NO_FILE_WAS_UPLOADED: 'E_NO_FILE_WAS_UPLOADED',
  E_USER_DISABLED: 'E_USER_DISABLED',
  E_RELATION_FOUND: 'E_RELATION_FOUND',

  E_WRONG_PASSWORD: 'E_WRONG_PASSWORD',

  // Default error handler
  E_DEFAULT_HANDLER: 'INTERNAL_SERVER_ERROR',
  E_DEFAULT_MESSAGE: 'E_DEFAULT_MESSAGE',

  //Auth Error
  AUTH_FORBIDDEN: 'AUTH_FORBIDDEN',
  AUTH_TOKEN_EXPIRED: 'AUTH_TOKEN_EXPIRED',
  AUTH_TOKEN_ERROR: 'AUTH_TOKEN_ERROR',
  AUTH_REFRESH_TOKEN_EXPIRED: 'AUTH_REFRESH_TOKEN_EXPIRED',
  AUTH_TOKEN_REVOKED: 'AUTH_TOKEN_REVOKED',
  AUTH_NEED_REFRESH_TOKEN: 'AUTH_NEED_REFRESH_TOKEN',

  AUTH_REFRESH_TOKEN_EXPIRED_IN_SECONDS: 2592000, // 30d

  // Order
  E_ORDER_CURRENT_STATUS_INVALID: 'E_ORDER_CURRENT_STATUS_INVALID',

  // HTTP STATUS CODE
  SUCCESS: 200,
  BAD_REQUEST: 400, // The server did not understand the request.
  UNAUTHORIZED: 401, // The requested page needs a username and a password.
  FORBIDDEN: 403, // Access is forbidden to the requested page.
  INTERNAL_SERVER_ERROR: 500, // Access is forbidden to the requested page.

}

// For JWT lib translate
ERROR_CONST.JWT_ERROR = {
  TokenExpiredError: {
    code: ERROR_CONST.AUTH_TOKEN_EXPIRED,
    status: ERROR_CONST.UNAUTHORIZED
  },
  JsonWebTokenError: {
    code: ERROR_CONST.AUTH_TOKEN_ERROR,
    status: ERROR_CONST.FORBIDDEN
  },
  default: {
    code: ERROR_CONST.E_INVALID_TOKEN,
    status: ERROR_CONST.FORBIDDEN
  }
}

module.exports.constants = ERROR_CONST

// TokenExpiredError
