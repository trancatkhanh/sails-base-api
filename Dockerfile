FROM node:10-alpine
# Set /app as workdir
RUN apk add --no-cache git bash make gcc g++ python tzdata

ENV TZ=Asia/Saigon
ENV NODE_ENV=production
WORKDIR /app
RUN mkdir -p /app/.tmp/public/uploads/images
COPY package*.json ./
RUN npm i -q --no-optional
COPY . .
#CMD ["npm","start"]
#CMD ["bash", "./docker_start.sh"]
CMD ["npm", "run", "start"]
